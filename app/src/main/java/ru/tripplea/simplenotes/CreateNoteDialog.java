package ru.tripplea.simplenotes;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ru.tripplea.simplenotes.database.DatabaseHelper;
import ru.tripplea.simplenotes.database.model.Note;

/**
 * Dialog for creating new note or changing existed note
 */
public class CreateNoteDialog extends DialogFragment {
    private static final String ARG_ID = "id";
    private static final String ARG_TITLE = "title";
    private static final String ARG_BODY = "body";

    private Integer id;

    /**
     * method for create instance of dialog's fragment with bundle args
     * @param note note's model
     * @return created dialog
     */
    public static CreateNoteDialog createDialog(Note note) {
        CreateNoteDialog fragment = new CreateNoteDialog();
        Bundle args = new Bundle();
        if (note.getId() != null) {
            args.putInt(ARG_ID, note.getId());
        }
        args.putString(ARG_TITLE, note.getTitle());
        args.putString(ARG_BODY, note.getBody());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_createnote, null);
        Bundle args = getArguments();
        if (args.containsKey(ARG_ID)) {
            id = args.getInt(ARG_ID);
        }
        String title = args.getString(ARG_TITLE);
        String body = args.getString(ARG_BODY);
        ((EditText)view.findViewById(R.id.title)).setText(title);
        ((EditText)view.findViewById(R.id.body)).setText(body);
        builder.setView(view)
                .setPositiveButton(id == null ? R.string.create : R.string.update, (dialog, id) -> {
                    DatabaseHelper db = new DatabaseHelper(getActivity());
                    Note note = new Note();
                    if (this.id != null) {
                        note = db.getNote(this.id);
                        note.setTitle(((EditText)view.findViewById(R.id.title)).getText().toString());
                        note.setBody(((EditText)view.findViewById(R.id.body)).getText().toString());
                        db.updateNote(note);
                    }
                    else {
                        note.setTitle(((EditText)view.findViewById(R.id.title)).getText().toString());
                        note.setBody(((EditText)view.findViewById(R.id.body)).getText().toString());
                        note.setId(db.addNote(note));
                    }
                    if (((CheckBox)view.findViewById(R.id.timer)).isChecked()) {
                        Note finalNote = note;
                        final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
                        executor.schedule(() -> {
                            db.deleteNote(finalNote);
                            Handler mainHandler = new Handler(Looper.getMainLooper());
                            Runnable myRunnable = () -> {
                                ((MainActivity) view.getContext()).reload();
                            };
                            mainHandler.post(myRunnable);
                        }, 5, TimeUnit.SECONDS);
                    }
                    ((MainActivity)view.getContext()).reload();
                })
                .setNeutralButton(R.string.delete, (dialog, id) -> {
                    if (this.id == null) {
                        dialog.cancel();
                    }
                    else {
                        DatabaseHelper db = new DatabaseHelper(getActivity());
                        Note note = new Note();
                        note.setId(this.id);
                        db.deleteNote(note);
                        ((MainActivity)view.getContext()).reload();
                    }
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> dialog.cancel());
        return builder.create();
    }
}
